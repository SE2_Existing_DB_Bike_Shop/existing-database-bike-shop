<?php
session_start();
$role = $_SESSION['role'];
?>
<html>
<head>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<style>
.topnav {
    margin: 20px;
    border-bottom: 3px solid #66fcf1;
    width: 100%;
    margin-left: 0px;
    margin-top: 0px;
    background: #0b0c10;
}
.inner-nav {
    padding: 20px;
}
.navlink {
    margin-left: 20px;
}
.button {
  padding: 16px 32px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
  margin: 4px 2px;
  -webkit-transition-duration: 0.4s; /* Safari */
  transition-duration: 0.4s;
  cursor: pointer;
  background-color: white;
  color: black !important;
  border: 2px solid #66fcf1;
}
.button:hover {
  background-color: #66fcf1;
  color: black !important;
  font-weight: bold;
}
img.logo {
    border-radius: 38px;
    float: left;
    margin-bottom: 10px;
    margin-right: 15px;
}
</style>
</head>
<body>
<div class="topnav">
	<div class="inner-nav">
	  <img class="logo" src="https://i.imgur.com/coREYYo.png" />
	  <!--Common Links-->
	  <a class="active navlink button" href="/">Home</a>
	  <a class="navlink button" href="/remote/view_all_bikes_api.php">View Bikes</a>
<?php
	if($role == 'e' || $role == 'a')
	{
	  echo '<a class="navlink button" href="/employee.php?schedule">Schedule</a>';
	  echo '<a class="navlink button" href="#sales">Sales</a>';
	}
	if($role == 'a')
	{
      echo '<a class="navlink button" href="/remote/createbike.php">Create Bike</a>';
	  echo '<a class="navlink button" href="/remote/deletebike.html">Delete Bike</a>';
	  echo '<a class="navlink button" href="#">Edit Bikes</a>';
	  echo '<a class="navlink button" href="#">View Customers</a>';
	  echo '<a class="navlink button" href="#">Edit Customers</a>';
	}
?>
	  <!-- Logout -->
	  <a class="navlink button" href="index.php" onclick="session_unset()">Log Out</a>
        </div>
</div>
