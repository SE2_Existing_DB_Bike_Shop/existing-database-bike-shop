<style>
/* Bordered form */

form {
  background-color: #F5F5F5;
  border: 3px solid #f1f1f1;
}

/* Full-width inputs */
input[type=text], input[type=password] {
  width: 100%;
  padding: 12px 20px;
  margin: 8px 0;
  display: inline-block;
  border: 1px solid #ccc;
  box-sizing: border-box;
}

/* Set a style for all buttons */
button {
  background-color: #0B0C10;
  font-size: 16px;
  font-weight: bold;
  padding: 14px 20px;
  margin: 8px 0;
  border: none;
  cursor: pointer;
  width: 100%;
}

/* Add a hover effect for buttons */
button:hover {
  opacity: 0.8;
}

/* Extra style for the cancel button (red) */
.cancelbtn {
  width: auto;
  padding: 10px 18px;
  background-color: #f44336;
}

/* Center the avatar image inside this container */
.imgcontainer {
  text-align: center;
  margin: 24px 0 12px 0;
}

/* Avatar image */
img.avatar {
  width: 20%;
  border-radius: 50%;
}

/* Add padding to containers */
.container {
  padding: 16px;
}

/* The "Forgot password" text */
span.psw {
  float: right;
  padding-top: 16px;
}

/* Change styles for span and cancel button on extra small screens */
@media screen and (max-width: 300px) {
  span.psw {
    display: block;
    float: none;
  }
  .cancelbtn {
    width: 100%;
  }
}
</style>
<body bgcolor="#0B0C10">
<h1 style="font-size:60px; color:#66FCF1;"  align="center">Welcome to Real Deal Bikes!</h1>
<h2 style="color:white" align="center">Where the bikes are definitely real</h2>
</body>
<form action="login.php" method="post">
  <div class="imgcontainer">
    <img src="assets/images/bicycle-clip-art-9.png" alt="Avatar" class="avatar">
  </div>
<div><a href="register.html">New here? Make an account!</a></div>
  <div class="container">
    <label for="email"><b>Email address</b></label>
    <input type="text" placeholder="Enter Email" name="email" required>

    <label for="password"><b>Password</b></label>
    <input type="password" placeholder="Enter Password" name="password" required>

    <button style="color:66FCF1" type="submit">Login</button>
  </div>

  <div class="container" style="background-color:#f1f1f1">
    <button style="color:white" type="button" class="cancelbtn">Cancel</button>
    <span class="psw">Forgot <a href="#">password?</a></span>
  </div>
</form>


<?php





?>
