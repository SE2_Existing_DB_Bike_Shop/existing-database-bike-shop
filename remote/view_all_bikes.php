<style>
/* Bordered form */
form {
  border: 3px solid #f1f1f1;
}

/* Full-width inputs */
input[type=text], input[type=password] {
  width: 100%;
  padding: 12px 20px;
  margin: 8px 0;
  display: inline-block;
  border: 1px solid #ccc;
  box-sizing: border-box;
}

/* Set a style for all buttons */
button {
  background-color: #4CAF50;
  color: white;
  padding: 14px 20px;
  margin: 8px 0;
  border: none;
  cursor: pointer;
  width: 100%;
}

/* Add a hover effect for buttons */
button:hover {
  opacity: 0.8;
}

/* Extra style for the cancel button (red) */
.cancelbtn {
  width: auto;
  padding: 10px 18px;
  background-color: #f44336;
}

/* Center the avatar image inside this container */
.imgcontainer {
  text-align: center;
  margin: 24px 0 12px 0;
}

/* Avatar image */
img.avatar {
  width: 40%;
  border-radius: 50%;
}

/* Add padding to containers */
.container {
  padding: 16px;
}

/* The "Forgot password" text */
span.psw {
  float: right;
  padding-top: 16px;
}

/* Change styles for span and cancel button on extra small screens */
@media screen and (max-width: 300px) {
  span.psw {
    display: block;
    float: none;
  }
  .cancelbtn {
    width: 100%;
  }
}
/* Add a black background color to the top navigation */
.topnav {
  background-color: #333;
  overflow: hidden;
}

/* Style the links inside the navigation bar */
.topnav a {
  float: left;
  color: #f2f2f2;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
  font-size: 17px;
}

/* Change the color of links on hover */
.topnav a:hover {
  background-color: #ddd;
  color: black;
}

/* Add a color to the active/current link */
.topnav a.active {
  background-color: #4CAF50;
  color: white;
}

</style>
  <div class="jumbotron">
	<h1>BikeShop</h1>
</div>

<div class="topnav">
  <a href="#../customer.php">Home</a>
  <a href="#shop" class="active">Shop</a>
  <a href="#orders">Orders</a>
  <a href="#accountsettings">Account Settings</a>
  <a href="/index.php">Log Out</a>
</div>
<?php
include('../remoteconnection.php');
    $query = "SELECT * FROM BICYCLE;";
$sth = $conn->prepare($query);
	$sth->execute();
echo '<table align="center" style="font-size:20px" cellpadding="13">
	<tr>
		<th><u>Serial Number</u></th>
		<th><u>Model</u></th>
		<th><u>Store Availability</u></th>
		<th><u>Price</u></th>
	</tr>';
while($row = $sth->fetch()){
	echo '
		<tr>
			<td align="center">'.$row['SERIALNUMBER'].'</td>
			<td align="center">'.$row['MODELTYPE'].'</td>';
			echo '<td align="center"><a href="bikes_by_store.php?id=' .$row['STOREID'].'">'.$row['STOREID'].'</a></td>';
	echo'
			<td align="center">'.$row['LISTPRICE'].'</td>
		</tr>';
}
echo '
</table>';
?>
